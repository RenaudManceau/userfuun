package com.example.userfuun.repository;


import com.example.userfuun.entity.userFuun.UserFuun;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserFuunRepository extends JpaRepository<UserFuun, Long> {
    Optional<UserFuun> findByEmail(String email);
}
