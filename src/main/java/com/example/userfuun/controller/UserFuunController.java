package com.example.userfuun.controller;

import com.example.userfuun.entity.userFuun.UserFuun;
import com.example.userfuun.entity.userFuun.UserFuunAuthImp;
import com.example.userfuun.payload.response.JwtResponse;
import com.example.userfuun.repository.UserFuunRepository;
import com.example.userfuun.security.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/userFuun")
public class UserFuunController {

    final UserFuunRepository userFuunRepository;
    final PasswordEncoder encoder;
    final AuthenticationManager authenticationManager;
    final JwtUtils jwtUtils;

    public UserFuunController(
           UserFuunRepository userFuunRepository,
           PasswordEncoder encoder,
           @Qualifier("userFuun_manager") AuthenticationManager authenticationManager,
           JwtUtils jwtUtils)
    {
        this.userFuunRepository = userFuunRepository;
        this.encoder = encoder;
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticate(String email, String password) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(email, password));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserFuunAuthImp userFuunAuthImp = (UserFuunAuthImp) authentication.getPrincipal();

        String jwt = jwtUtils.generateJwtToken(userFuunAuthImp.getUsername());

        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(String name, String email, String password) {
        //TODO: check si user n'existe pas

        try {
            UserFuun newUserFuun = new UserFuun(
                    email,
                    encoder.encode(password)
            );
            userFuunRepository.save(newUserFuun);
            return new ResponseEntity<>(
                    "C'est ok",
                    HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(
                    "C'est pas ok",
                    HttpStatus.BAD_REQUEST);
        }
    }
}
