package com.example.userfuun.entity.userFuun;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.Collection;
import java.util.Objects;

public class UserFuunAuthImp implements UserDetails {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;
    private String email;
    private String username;
    @JsonIgnore
    private String password;

    public Collection<? extends GrantedAuthority> authorities;

    public UserFuunAuthImp(Long id, String email, String password)
    {
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public static UserFuunAuthImp build(UserFuun userFuun)
    {
        return new UserFuunAuthImp(
                userFuun.getId(),
                userFuun.getEmail(),
                userFuun.getPassword()
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserFuunAuthImp userFuun = (UserFuunAuthImp) o;
        return Objects.equals(id, userFuun.id);
    }

}
